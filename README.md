﻿**Experis Academy, Norway**

**Authors:**
* **Ørjan Storås**

# Experis Week3 Task 23
## Task 23


- [x] Write or pirate the HTML to display a Form which would be used to gather information about a person. Just first name, last name, email.
- [x] Style it with a CSS class (Style it however you want)
- [x] Hand in the HTML file and CSS file
